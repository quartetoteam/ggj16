﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{

	public StateManager manager;
	public List<SummonerController> summoners= new List<SummonerController>();
	public InputManager input;
	public SummonerController prefabSummoner;

	bool completed = false;

	public int GetAliveSummoners(){
		int i = 0;
		foreach (SummonerController s in summoners) {
			if (!s.IsAlive ())
				i++;
		}
//		Debug.Log ("Summoners: "+ i);
		return i;
	}


	void Start(){
		
	}


	void Update(){

		if (GameController.instance.ritual.IsCompleted () && !completed) {
			completed = true;
//			if (!GameController.instance.ritual.WinPlayer ()) {
//				manager.anim.SetTrigger ("dead");
//			} else {
//				manager.anim.SetTrigger ("win");
//			}
		}
		switch (input.GetAction ()) {
		case PlayerActions.LEFTBLOCK:
			AudioManager.soundPlayer (AudioManager.soundType.sblock);
			manager.ChangeState (EState.blockLeft);
			foreach (SummonerController s in summoners) {
				if (!s.IsAlive ())
					s.InvokeDefense ("left");
			}
			break;
		case PlayerActions.RIGHTBLOCK:
			AudioManager.soundPlayer (AudioManager.soundType.sblock);
			manager.ChangeState (EState.blockRight);
			foreach (SummonerController s in summoners) {
				if (!s.IsAlive ())
					s.InvokeDefense ("right");
			}
			break;
		case PlayerActions.LEFTKICK:
			AudioManager.soundPlayer (AudioManager.soundType.lattack);
			manager.ChangeState (EState.attackLeft);
			foreach (SummonerController s in summoners) {
				if (!s.IsAlive ())
					s.InvokeAttack ("left");
			}break;
		case PlayerActions.RIGHTKICK:
			AudioManager.soundPlayer (AudioManager.soundType.rattack);
			manager.ChangeState (EState.attackRight);
			foreach (SummonerController s in summoners) {
				if (!s.IsAlive ())
					s.InvokeAttack ("right");
			}break;
		case PlayerActions.BIGBLOCK:
			manager.ChangeState (EState.special);
			AudioManager.soundPlayer (AudioManager.soundType.cblock);
			foreach (SummonerController s in summoners) {
				if (!s.IsAlive ())
					s.InvokeAttack ("special");
			}break;
		case PlayerActions.BIGBLOCKING:
		case PlayerActions.LEFTBLOCKING:
		case PlayerActions.RIGHTBLOCKING:
			break;
		case PlayerActions.EMPTY:
			if(manager.currEnumState != EState.attackLeft && manager.currEnumState != EState.attackRight)
			manager.ChangeState (EState.idle);
			break;
		}
	}
}

