﻿using UnityEngine;

public enum PlayerActions
{
    EMPTY,
    LEFTKICK,
    RIGHTKICK,
    LEFTBLOCK,
    LEFTBLOCKING,
    RIGHTBLOCK,
    RIGHTBLOCKING,
    BIGBLOCK,
    BIGBLOCKING
}

public class InputManager : MonoBehaviour
{
    private PlayerActions action;
    private float current_time;
    [Tooltip("Time until block is completed.")]
    public float delayBlockingTime;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            current_time = Time.time;
            action = PlayerActions.LEFTBLOCK;
        }
        else if (Input.GetKey(KeyCode.LeftArrow) && Time.time >= current_time + delayBlockingTime)
        {
            action = PlayerActions.LEFTBLOCKING;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            current_time = Time.time;
            action = PlayerActions.RIGHTBLOCK;
        }
        else if (Input.GetKey(KeyCode.RightArrow) && Time.time >= current_time + delayBlockingTime)
        {
            action = PlayerActions.RIGHTBLOCKING;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            current_time = Time.time;
            action = PlayerActions.BIGBLOCK;
        }
        else if (Input.GetKey(KeyCode.DownArrow) && Time.time >= current_time + delayBlockingTime)
        {
            action = PlayerActions.BIGBLOCKING;
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            action = PlayerActions.LEFTKICK;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            action = PlayerActions.RIGHTKICK;
        }
    }

    //- Retorna la última acción que ha pulsado el usuario ---------------------------------------
    public PlayerActions GetAction()
    {
        PlayerActions a = action;
//        Debug.Log("Player action: " + action);
        action = PlayerActions.EMPTY;
        return a;
    }
}
