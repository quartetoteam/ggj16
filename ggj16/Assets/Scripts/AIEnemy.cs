﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIEnemy : MonoBehaviour {

	public StateManager manager;
	public GUIManager gui;

	StateManager playerManager;

	List<EState> current_combo = new List<EState>();
	int currComboAction = 0;

	bool launchingAction=false;
	bool launchingCombo=false;
	float timeToNextAction = 4;
	bool completed = false;

	// Use this for initialization
	void Start () {
		playerManager = GameController.instance.player.manager;
		current_combo.Add (EState.idle);
	}
	
	// Update is called once per frame
	void Update () {
		if (!gui.IsRunning())
			return;

		if (GameController.instance.ritual.IsCompleted () && !completed) {
			completed = true;
			if (GameController.instance.ritual.WinPlayer ()) {
				manager.anim.SetTrigger ("dead");
			} else {
				manager.anim.SetTrigger ("win");
			}
		}
		if (launchingAction) {
//			Debug.Log ("LaunchingAction!! managerState: "+manager.currEnumState +" currCombo:" +current_combo [currComboAction]+ " anim: " + manager.anim.name );
			if (manager.currEnumState == EState.idle && !manager.anim.IsInTransition(0)) {
//				Debug.Log ("ActionReached");
				launchingAction = false;
				currComboAction++;
//				Debug.Break ();
				if (current_combo.Count <= currComboAction) {
//					Debug.Log ("NextCombo!!");
					launchingCombo = false;
					//TODO: siguiente combo
//					current_combo = Configuration.combos [Random.Range (0, Configuration.combos.Count)];
//					currComboAction = 0;
//					timeToNextAction = Random.Range (Configuration.MIN_COMBO_T, Configuration.MAX_COMBO_T);
//					Debug.Log ("TimeNextAction:" + timeToNextAction);
				}
			} else {
				if (manager.currEnumState == EState.blockLeft || manager.currEnumState == EState.blockRight) {
					if (timeToNextAction > 0)
						timeToNextAction -= Time.deltaTime;
					else 
					{
						launchingCombo = false;
						launchingAction = false;
						manager.ChangeState(EState.idle);
					}
				}
				//manager.ChangeState (current_combo [currComboAction]);
			}
		}else{
			if (!launchingCombo) {
				timeToNextAction -= Time.deltaTime;
				if (timeToNextAction <= 0) {
					current_combo = Configuration.combos [Random.Range (0, Configuration.combos.Count)];
					currComboAction = 0;
					timeToNextAction = Random.Range (Configuration.MIN_COMBO_T, Configuration.MAX_COMBO_T);
					launchingCombo = true;
				}
				if (manager.currEnumState == EState.idle) {
					if ((playerManager.currEnumState == EState.attackLeft || playerManager.currEnumState == EState.attackRight) &&
						//					   timeToNextAction == 0 &&
						manager.currEnumState != EState.blockLeft &&
						manager.currEnumState != EState.blockRight) {
						timeToNextAction = Random.Range (Configuration.MIN_ENEMY_T, Configuration.MAX_ENEMY_T);
						currComboAction = 0;
						if (playerManager.currEnumState == EState.attackLeft) {
							current_combo = new List<EState> (){ EState.blockRight };
						} else {
							current_combo = new List<EState> (){ EState.blockLeft };
						}

					}
				}
//				Debug.Log ("No combo, no party!!");
//				Debug.Log ( "NextState: " + manager.anim.GetNextAnimatorStateInfo (0).IsTag("idle"));
//				Debug.Log ("TimeLeft"+timeToNextAction);
			} else {
				if (manager.currEnumState == EState.idle) {
					if ((playerManager.currEnumState == EState.attackLeft || playerManager.currEnumState == EState.attackRight) &&
//					   timeToNextAction == 0 &&
						manager.currEnumState != EState.blockLeft &&
						manager.currEnumState != EState.blockRight) {
						timeToNextAction = Random.Range (Configuration.MIN_ENEMY_T, Configuration.MAX_ENEMY_T);
						currComboAction = 0;
						if (playerManager.currEnumState == EState.attackLeft) {
							current_combo = new List<EState> (){ EState.blockRight };
						} else {
							current_combo = new List<EState> (){ EState.blockLeft };
						}

					} else {
						LaunchNextAction ();
					}
				}
			}
		}

	}

	private void LaunchNextAction(){
		if (current_combo.Count <= currComboAction)
			return;
//		launchingCombo = true;
//		Debug.Log ("NextAction: "+current_combo [currComboAction].ToString());
		manager.ChangeState (current_combo [currComboAction]);
		launchingAction = true;
	}
}
