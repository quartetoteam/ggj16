﻿using UnityEngine;

public class RitualController : MonoBehaviour
{
    public ParticleSystem[] fires;
    private int power;

    void Start()
    {
        power = 50;
        foreach (ParticleSystem fire in fires) fire.startLifetime = 0.64f;
    }

    //- Aumenta o disminuye el progreso del ritual -----------------------------------------------
    //- @is_player: TRUE si lo incrementa el player, FALSE si es el boss
    //- @pwr: Potencia del incremento
    public void SetRitualProgress(bool is_player, int pwr)
    {
        float fpower;
        if (!IsCompleted())
        {
            power = (is_player) ? (power + pwr) : (power - pwr);
            fpower = power;
            foreach (ParticleSystem fire in fires)
            {
                fire.startLifetime = 0.3f + 2 * fpower / 100;
                fire.startSize = (power > 50) ? 1 + 2 * fpower / 1000 : 1 - 2 * fpower / 1000;
            }
        }
    }

    //- Mira si esta el ritual completo por algun bando ------------------------------------------
    public bool IsCompleted()
    {
        return (power >= 100) || (power <= 0);
    }

    //- Mira si ha ganado el player --------------------------------------------------------------
    public bool WinPlayer()
    {
        return power >= 100;
    }
}