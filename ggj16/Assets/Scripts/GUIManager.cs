﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour
{
    public GameObject WinPanel;
    public GameObject LosePanel;
    public GameObject BackButton;
    public RawImage Fade;

    public RitualController ritual;
    public Transform menuPosition;
    public Transform gamePosition;

    private bool in_game;
	private bool _once = false;

    void Start()
    {
        Fade.enabled = true;
        Camera.main.transform.position = menuPosition.position;
        Camera.main.transform.rotation = menuPosition.rotation;
        StartCoroutine("FadeControl", "in");
        in_game = false;
    }

    void Update()
    {
        if (ritual.IsCompleted() && !_once)
        {
            if (ritual.WinPlayer())
            {
				WinPanel.SetActive(true);
            }
            else
            {
				LosePanel.SetActive(true);
            }
			_once = true;
        }
    }

    //- Hace un fade -----------------------------------------------------------------------------
    private IEnumerator FadeControl(string direction)
    {
        float alpha = Fade.color.a;
        if (direction == "in")
        {
            while (Fade.color.a > 0)
            {
                alpha -= 0.01f;
                Fade.color = new Color(Fade.color.r, Fade.color.g, Fade.color.b, alpha);
                yield return null;
            }
        }
        else
        {
            while (Fade.color.a < 1)
            {
                alpha += 0.1f;
                Fade.color = new Color(Fade.color.r, Fade.color.g, Fade.color.b, alpha);
                yield return null;
            }
            SceneManager.LoadScene("Level-01");
        }
    }

    //- Empieza el juego -------------------------------------------------------------------------
    public void StartGame()
    {
        StartCoroutine("moveCameraTo", gamePosition);
        Invoke("StopCameraMovement", 5);
        Invoke("BackEnabled", 6);
    }

    //- Vuelve al menu principal -----------------------------------------------------------------
    public void GoMenu()
    {
        StartCoroutine("moveCameraTo", menuPosition);
        Invoke("StopCameraMovement", 5);
        StartCoroutine("FadeControl", "out");
        BackButton.SetActive(false);
    }
    
    //- Mueve la camara de donde esta al punto objetivo suavemente -------------------------------
    //- @target: Estado final --------------------------------------------------------------------
    private IEnumerator moveCameraTo(Transform target)
    {
        while (target != Camera.main.transform)
        {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, target.position, Time.deltaTime);
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, target.rotation, Time.deltaTime);
            yield return null;
        }
    }

    //- Para la coroutine de movimiento por seguridad --------------------------------------------
    private void StopCameraMovement()
    {
        StopCoroutine("moveCameraTo");
        in_game = !in_game;
    }

    //- Activa el botón de back ------------------------------------------------------------------
    private void BackEnabled()
    {
        BackButton.SetActive(true);
    }

    //- Cierra la aplicación ---------------------------------------------------------------------
    public void ExitGame()
    {
        Application.Quit();
    }

    //- Retorna cuando el juego esta corriendo ---------------------------------------------------
    public bool IsRunning()
    {
        return in_game;
    }
}