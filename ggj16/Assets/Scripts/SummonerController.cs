﻿using UnityEngine;

public class SummonerController : MonoBehaviour
{
    private bool dead;
    public Animator anim;

    void Start()
    {
        dead = false;
    }

    //- Mira si puede resucitar este invocador ----------------------------------------------------
    //- #return: TRUE si ha creado un nuevo invocador, FALSE si no ha creado ninguno --------------
    public bool NewSummoner()
    {
        if (IsAlive())
        {
//            Debug.Log("New summonmer!");
            dead = false;
            anim.SetTrigger("dead");
            AudioManager.soundPlayer (AudioManager.soundType.sborn);
			gameObject.SetActive (true);
            return true;
        }
        return false;
    }

    //- Respuesta al ser dañados -----------------------------------------------------------------
    //- @kill_prob: Probabilidad de matar a un invocador con el impacto con el ataque ------------
    //- #return: TRUE si el invocador ha muerto, FALSE si no ha muerto ---------------------------
    public bool DamagePlayer(int kill_prob)
    {
//        Debug.Log("Player damaged!");
        anim.SetTrigger("hit");
        if (Random.Range(0, 100) <= kill_prob)
        {
            KillSummoner();
            return true;
        }
        return false;
    }

    //- Mata al invocador ------------------------------------------------------------------------
    private void KillSummoner()
    {
        dead = true;
        anim.SetTrigger("dead");
//        Debug.Log("Kill summoner!");
		gameObject.SetActive(false);
		AudioManager.soundPlayer (AudioManager.soundType.sdie);
    }

    //- Activa la animación de defensa -----------------------------------------------------------
    //- @side: orientación de la defensa left/right ----------------------------------------------
    public void InvokeDefense(string side)
    {
        if (side == "left") anim.SetTrigger("defenseL");
        else                anim.SetTrigger("defenseR");
    }

    //- Activa la animación de ataque ------------------------------------------------------------
    //- @side: orientación del ataque left/special/right -----------------------------------------
    public void InvokeAttack(string side)
    {
        if (side == "left") anim.SetTrigger("attackL");
        else if (side == "special") anim.SetTrigger("attackS");
        else anim.SetTrigger("attackR");
    }

    //- Respuesta al ganar -----------------------------------------------------------------------
    public void PlayerWin()
    {
      //AudioManager.soundPlayer(AudioManager.soundType.pwin);
        anim.SetTrigger("win");
//        Debug.Log("Player wins! Summoners celebration!");
    }

    //- Retorna si el invocador esta vivo --------------------------------------------------------
    public bool IsAlive()
    {
        return dead;
    }
}
