﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Configuration
{
	public const int MAX_SUMMONERS = 10;
	public const int ENEMY_ATTACK = 5;//Valor de ataque normal del enemigo
	public const int ENEMY_SPECIAL_ATTACK = 15;//Valor del ataque especial del enemigo
	public const float MIN_ENEMY_T = 0.1f;//Tiempo minimo de respuesta del enemigo a un ataque
	public const float MAX_ENEMY_T = 0.6f;//Tiempo maximo de respuesta del enemigo a un ataque
	public const float MIN_COMBO_T = 1f;//Tiempo minimo de respuesta del enemigo a un ataque
	public const float MAX_COMBO_T = 4f;//Tiempo maximo de respuesta del enemigo a un ataque
	public const int KILL_PROB = 2;



	/// <summary>
	/// Lista de combos que puede lanzar el enemigo
	/// </summary>
	public static List<List<EState>> combos = new List<List<EState>>(){
		new List<EState>(){ EState.attackLeft },
		new List<EState>(){EState.attackRight},
		new List<EState>(){EState.special},
		new List<EState>(){EState.attackLeft,EState.attackRight},
		new List<EState>(){EState.attackRight,EState.attackLeft},
		new List<EState>(){EState.attackLeft,EState.attackLeft,EState.attackRight},
		new List<EState>(){EState.attackRight,EState.attackRight,EState.attackLeft},
		new List<EState>(){EState.attackLeft,EState.special},
		new List<EState>(){EState.attackRight,EState.special},
		new List<EState>(){EState.attackLeft,EState.attackLeft,EState.attackRight,EState.attackRight},
		new List<EState>(){EState.attackRight,EState.attackRight,EState.attackLeft,EState.attackLeft},
		new List<EState>(){EState.attackLeft,EState.attackLeft,EState.attackRight,EState.attackLeft},
		new List<EState>(){EState.attackRight,EState.attackRight,EState.attackLeft,EState.attackRight},
		new List<EState>(){EState.attackLeft,EState.attackLeft,EState.special},
		new List<EState>(){EState.attackRight,EState.attackRight,EState.special}
	};

}

