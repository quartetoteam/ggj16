﻿using UnityEngine;
using System.Collections;

public class Utils
{

	public static void InstantiateAndDestroy(GameObject prefab, float t=1f){
		GameObject aux = GameObject.Instantiate (prefab);
		GameObject.Destroy (aux, t);
	}
}

