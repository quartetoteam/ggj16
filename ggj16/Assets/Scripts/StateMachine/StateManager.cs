﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateManager : MonoBehaviour {
	
	StateBase current_state;
	public EState currEnumState;
	public bool isPlayer;
    [HideInInspector]
	public Animator anim;
	public EState lastEnumState;

	Dictionary<EState,StateBase> states = new Dictionary<EState, StateBase>();


	// Use this for initialization
	void Start () 
	{
		anim = this.gameObject.GetComponent<Animator> ();
		Init ();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (current_state != null) {
//			Debug.Log (gameObject.name +" idle: "+ anim.GetCurrentAnimatorStateInfo(0).IsTag("idle"));
			current_state.OnUpdateState ();
		}
	}

	void Init ()
	{


		states.Add (EState.idle, new StateIdle (this));
		states.Add (EState.hit, new StateHit (this));
		states.Add (EState.special, new StateSpecial (this));
		states.Add (EState.attackLeft, new StateAttackLeft (this));
		states.Add (EState.attackRight, new StateAttackRight (this));
		states.Add (EState.blockLeft, new StateBlockLeft (this));
		states.Add (EState.blockRight, new StateBlockRight (this));


		current_state = states [EState.idle];
		currEnumState = EState.idle;

	}

	public void ChangeState(EState _state)
	{
		if (_state == currEnumState)
			return;
		//		Debug.Log (gameObject.name+" NewState: "+_state.ToString());
		current_state = states [_state];
//		current_state.OnExitState ();
		lastEnumState = currEnumState;
		currEnumState = _state;
		states [_state].OnEnterState ();
//		current_state.OnEnterState ();
	}

	public StateBase GetCurrentState(){
		return states[currEnumState];
	}

	public bool isBlockingLeft(){
		bool ret = false;
		EState state = (currEnumState == EState.blockLeft || lastEnumState == EState.blockLeft) ? EState.blockLeft : EState.idle;
		state = currEnumState;
		if (state == EState.blockLeft && states[state].completed)
			ret = true;
		return ret;
	}

	public bool isBlockingRight(){
		bool ret = false;
		EState state = (currEnumState == EState.blockRight || lastEnumState == EState.blockRight) ? EState.blockRight : EState.idle;
		state = currEnumState;
		if (state == EState.blockRight && states[state].completed)
			ret = true;
		return ret;
	}

	public bool isBlockingSpecial(){
		bool ret = false;
		EState state = (currEnumState == EState.special || lastEnumState == EState.special) ? EState.special : EState.idle;
		state = currEnumState;
		if (state == EState.special && states[state].completed)
			ret = true;
		return ret;
	}

	public void ResetAllTriggers(){
//		anim.ResetTrigger ("idle");
//		anim.ResetTrigger ("attackLeft");
//		anim.ResetTrigger ("attackRight");
//		anim.ResetTrigger ("blockLeft");
//		anim.ResetTrigger ("blockRight");
//		anim.ResetTrigger ("hit");
//		anim.ResetTrigger ("special");
//		anim.ResetTrigger ("pene");
	}
}
