﻿using UnityEngine;
using System.Collections;

public class AnimationEventHandler : MonoBehaviour {


	public StateManager manager;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CompleteAction(){
		if (manager == null)
			return;
//		Debug.Log ("CompleteAction state:"+manager.GetCurrentState ().ToString());
		manager.GetCurrentState ().completed = true;
		GameController.instance.CheckHit (manager.isPlayer);
	}

	public void Complete(){
		if (manager == null)
			return;
		manager.GetCurrentState ().completed = true;
	}
}
