﻿using UnityEngine;
using System.Collections;

public class StateSpecial : StateBase {

	bool _once = false;
	bool didEnter = false;
	public StateSpecial (StateManager m) : base(m){
	}

	public override void OnEnterState ()
	{
		base.OnEnterState ();
		_once = false;
		didEnter = false;
		manager.anim.SetTrigger ("special");
		
	}

	public override void  OnExitState(){
		base.OnExitState ();
	}

	public override void OnUpdateState(){
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("special") && !manager.anim.IsInTransition (0))
			didEnter = true;
		base.OnUpdateState ();
		if (completed && !_once) {
			_once = true;
			GameController.instance.CheckHit (manager.isPlayer);
		}
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && 
//			manager.anim.GetNextAnimatorStateInfo(0).IsTag("idle") &&
			!manager.anim.IsInTransition(0) && didEnter)
			manager.ChangeState (EState.idle);
	}
}
