﻿using UnityEngine;
using System.Collections;

public enum EState {
	idle,
	hit,
	special,
	blockLeft,
	blockRight,
	attackLeft,
	attackRight
}

public abstract class StateBase {


	protected StateManager manager;

	public bool completed;

	public StateBase(StateManager m){
		manager = m;
	}

	public virtual void OnEnterState(){
		completed = false;
//		manager.ResetAllTriggers ();
	}

	public virtual void OnExitState(){
	}

	public virtual void OnUpdateState(){
	}
}
