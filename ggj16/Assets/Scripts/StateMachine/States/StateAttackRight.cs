﻿using UnityEngine;
using System.Collections;

public class StateAttackRight : StateBase {
	
	bool _once = false;
	bool didEnter = false;

	public StateAttackRight (StateManager m) : base(m){
	}

	public override void OnEnterState ()
	{
//		Debug.Log ("RightEnter");
		base.OnEnterState ();
		manager.anim.ResetTrigger ("idle");
		manager.anim.SetTrigger ("attackRight");
		_once = false;
		didEnter = false;
	}

	public override void  OnExitState(){
		base.OnExitState ();
	}

	public override void OnUpdateState(){
		base.OnUpdateState ();

		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("attackRight") && !manager.anim.IsInTransition (0))
			didEnter = true;
		if (completed && !_once) {
			_once = true;
			GameController.instance.CheckHit (manager.isPlayer);
		}
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && 
//			manager.anim.GetNextAnimatorStateInfo(0).IsTag("idle") &&
			!manager.anim.IsInTransition(0) && didEnter)
			manager.ChangeState (EState.idle);
	}
}
