﻿using UnityEngine;
using System.Collections;

public class StateBlockRight : StateBase {


	bool didEnter = false;
	public StateBlockRight (StateManager m) : base(m){
	}

	public override void OnEnterState ()
	{
		base.OnEnterState ();
		didEnter = false;
		manager.anim.SetTrigger ("blockRight");
	}

	public override void  OnExitState(){
		base.OnExitState ();
	}

	public override void OnUpdateState(){
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("blockRight") && !manager.anim.IsInTransition (0))
			didEnter = true;
		base.OnUpdateState ();
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && !manager.anim.IsInTransition(0) && didEnter)
			manager.ChangeState (EState.idle);
	}
}
