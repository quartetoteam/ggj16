﻿using UnityEngine;
using System.Collections;

public class StateHit : StateBase {


	bool didEnter = false;
	public StateHit (StateManager m) : base(m){
	}

	public override void OnEnterState ()
	{
		base.OnEnterState ();
		didEnter = false;
		if (manager.lastEnumState != EState.idle)
			manager.anim.SetTrigger ("idle");
		manager.anim.SetTrigger ("hit");
	}

	public override void  OnExitState(){
		base.OnExitState ();
	}

	public override void OnUpdateState(){
		base.OnUpdateState ();
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("hit") && !manager.anim.IsInTransition (0))
			didEnter = true;
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && !manager.anim.IsInTransition(0) && didEnter)
			manager.ChangeState (EState.idle);
	}
}
