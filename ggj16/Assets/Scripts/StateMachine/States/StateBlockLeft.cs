﻿using UnityEngine;
using System.Collections;

public class StateBlockLeft : StateBase {


	public StateBlockLeft (StateManager m) : base(m){
	}

	bool didEnter = false;
	public override void OnEnterState ()
	{
		base.OnEnterState ();
		didEnter = false;
		manager.anim.SetTrigger ("blockLeft");
	}

	public override void  OnExitState(){
		base.OnExitState ();
	}

	public override void OnUpdateState(){
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("blockLeft") && !manager.anim.IsInTransition (0))
			didEnter = true;
		base.OnUpdateState ();
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && !manager.anim.IsInTransition(0) && didEnter)
			manager.ChangeState (EState.idle);
	}
}
