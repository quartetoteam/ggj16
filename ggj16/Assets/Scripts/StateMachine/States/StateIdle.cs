﻿using UnityEngine;
using System.Collections;

public class StateIdle : StateBase
{

	AudioSource audio;

	public StateIdle (StateManager m) : base (m)
	{
		if (!manager.isPlayer) {
			audio = manager.gameObject.GetComponent<AudioSource> ();
		}
	}

	public override void OnEnterState ()
	{
		base.OnEnterState ();
//		manager.ResetAllTriggers ();
		if (!manager.isPlayer) {
			audio.Play ();
		}
	}

	public override void  OnExitState ()
	{
		base.OnExitState ();
		if (!manager.isPlayer) {
			audio.Stop ();
		}
	}

	public override void OnUpdateState ()
	{
		base.OnUpdateState ();
		if (!manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && !manager.anim.IsInTransition (0))
			manager.anim.SetTrigger ("idle");
		if (manager.anim.GetCurrentAnimatorStateInfo (0).IsTag ("idle") && !manager.anim.IsInTransition (0))
			manager.anim.ResetTrigger ("idle");
	}

}
