﻿using UnityEngine;
using System.Collections;

public class GameController: MonoBehaviour 
{

//	public StateManager publiclayer;
	public Player player;
	public StateManager enemy;
	public RitualController ritual;

	public GameObject prefabVFXEnemyBlock;
	public GameObject prefabVFXEnemyHit;
	public GameObject prefabVFXPlayerBlock;
	public GameObject prefabVFXPlayerHit;

	public static GameController instance;


	bool _once = false;
	void Awake() {
		if (instance == null) {
			instance = this;
//			DontDestroyOnLoad (gameObject);
		} else {
//			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (ritual.IsCompleted () & !_once) {
			if (ritual.WinPlayer ()) {
				AudioManager.soundPlayer (AudioManager.soundType.pwin);
				AudioManager.soundPlayer (AudioManager.soundType.ddie);
				foreach (SummonerController sc in player.summoners) {
					if (!sc.gameObject.activeSelf)
						sc.gameObject.SetActive (true);
					sc.anim.SetTrigger ("win");
				}
			} else {
				AudioManager.soundPlayer (AudioManager.soundType.dwin);
			}
			_once = true;
		}
	}


	public void CheckHit (bool isPlayer){
//		Debug.Log ("CheckHit");
		EState playerState = player.manager.currEnumState;
		EState enemyState = enemy.currEnumState;

		if (isPlayer) {
			if (playerState == EState.attackLeft && !enemy.isBlockingRight ()) {
				//Enemy recibe una hostia de izquierdas
				if (!enemy.isBlockingRight ()) {
					ritual.SetRitualProgress (true, player.GetAliveSummoners ());
					enemy.ChangeState (EState.hit);
					Utils.InstantiateAndDestroy (prefabVFXEnemyHit);
					AudioManager.soundPlayer (AudioManager.soundType.dhit);
				} else {
					Utils.InstantiateAndDestroy (prefabVFXEnemyBlock);
					AudioManager.soundPlayer (AudioManager.soundType.bhit);
				}
			} else if (playerState == EState.attackRight && !enemy.isBlockingLeft ()) {
				//Enemy recibe una hostia de derechas
				if (!enemy.isBlockingLeft ()) {
					ritual.SetRitualProgress (true, player.GetAliveSummoners ());
					enemy.ChangeState (EState.hit);
					Utils.InstantiateAndDestroy (prefabVFXEnemyHit);
					AudioManager.soundPlayer (AudioManager.soundType.dhit);
				} else {
					Utils.InstantiateAndDestroy (prefabVFXEnemyBlock);
					AudioManager.soundPlayer (AudioManager.soundType.bhit);
				}
			}
		} else {
			if (enemyState == EState.attackLeft) {
				//Player recibe una hostia de izquierdas
				if (!player.manager.isBlockingRight ()) {
					ritual.SetRitualProgress (false, Configuration.ENEMY_ATTACK);
					player.manager.ChangeState (EState.hit);
					foreach (SummonerController s in player.summoners) {
						s.DamagePlayer (Configuration.KILL_PROB);
					}
					Utils.InstantiateAndDestroy (prefabVFXPlayerHit);
					AudioManager.soundPlayer (AudioManager.soundType.phit);
				} else {
					Utils.InstantiateAndDestroy (prefabVFXPlayerBlock);
					AudioManager.soundPlayer (AudioManager.soundType.bhit);
				}
			} else if (enemyState == EState.attackRight) {
				//Player recibe una hostia de derechas
				if (!player.manager.isBlockingLeft ()) {
					ritual.SetRitualProgress (false, Configuration.ENEMY_ATTACK);
					player.manager.ChangeState (EState.hit);
					foreach (SummonerController s in player.summoners) {
						s.DamagePlayer (Configuration.KILL_PROB);
					}
					Utils.InstantiateAndDestroy (prefabVFXPlayerHit);
					AudioManager.soundPlayer (AudioManager.soundType.phit);
				} else {
					Utils.InstantiateAndDestroy (prefabVFXPlayerBlock);
					AudioManager.soundPlayer (AudioManager.soundType.bhit);
				}
			} else if (enemyState == EState.special) {
				//Player Recibe una toña fuerte
				if (!player.manager.isBlockingSpecial ()) {
					ritual.SetRitualProgress (false, Configuration.ENEMY_SPECIAL_ATTACK);
					player.manager.ChangeState (EState.hit);
					foreach (SummonerController s in player.summoners) {
						s.DamagePlayer (10);
					}
					Utils.InstantiateAndDestroy (prefabVFXPlayerHit);
					AudioManager.soundPlayer (AudioManager.soundType.phit);
				} else {
					Utils.InstantiateAndDestroy (prefabVFXPlayerBlock);
					AudioManager.soundPlayer (AudioManager.soundType.bhit);
				}
			}
		}
	}
}

