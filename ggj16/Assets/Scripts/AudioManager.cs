﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public enum soundType {bhit, dhit, phit, sdie, sborn, pwin, dattack, ddie, didle, dwin, button, buttonmenu, cblock, lattack, rattack, sblock};

	static string bhitpath="Audio/blockedhit";//
	static string dhitpath="Audio/daemonreceives2";//
	static string phitpath="Audio/playerreceives";//
	static string sdiepath="Audio/summondie";//
	static string sbornpath="Audio/summonreborn";
	static string pwinpath="Audio/win";
	static string dattackpath="Audio/demonattack";
	static string ddiepath="Audio/demondie";
	static string didlepath="Audio/demonidle";
	static string dwinpath="Audio/demonwin";
	static string buttonpath="Audio/button";
	static string centralblockpath="Audio/pray_centralblockmulti";
	static string leftattackpath="Audio/pray_leftattackmulti";
	static string rightattackpath="Audio/pray_rightattackmulti";
	static string sideblockpath="Audio/pray_sideblockmulti";


	public static void soundPlayer (soundType st){

		string path="";
		Vector3 pos=new Vector3(0.0f, 14.92f, -31.03f); //per defecte pos cameracombat, hardcoded
		float vol = 1.0f; //volum per defecte

		switch (st) {
		case (soundType.bhit):
			path = bhitpath;
			break;

		case (soundType.dhit):
			path = dhitpath;
			vol = 0.6f;
			break;

		case (soundType.phit):
			path = phitpath;
			vol = 0.8f;
			break;
		
		case (soundType.sdie):
			path = sdiepath;
			break;

		case (soundType.sborn):
			path = sbornpath;
			break;

		case (soundType.pwin):
			path = pwinpath;
			break;

		case (soundType.dattack):
			path = dattackpath;
			break;

		case (soundType.ddie):
			path = ddiepath;
			break;

		case (soundType.didle):
			path = didlepath;
			break;

		case (soundType.dwin):
			path = dwinpath;
			break;

		case (soundType.button):
			path = buttonpath;
			vol = 0.4f;
			break;

		case (soundType.buttonmenu):
			path = buttonpath;
			vol = 0.4f;
			pos=new Vector3(0.0f, 16.6f, -25.5f);
			break;

		case (soundType.cblock):
			path = centralblockpath;
			vol = 0.4f;
			break;

		case (soundType.lattack):
			path = leftattackpath;
			vol = 0.4f;
			break;

		case (soundType.rattack):
			path = rightattackpath;
			vol = 0.4f;
			break;

		case (soundType.sblock):
			path = sideblockpath;
			vol = 0.4f;
			break;

		}


		AudioClip aClip = Resources.Load (path, typeof(AudioClip)) as AudioClip;
		AudioSource.PlayClipAtPoint(aClip,pos, vol);
	}


	public void playButtonSound(){
		soundPlayer (soundType.button);
	}


	public void playButtonSoundMenu(){
		soundPlayer (soundType.buttonmenu);
	}


}


